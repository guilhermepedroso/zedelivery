/*
Copyright (c) 2010,2011,2012,2013,2014 Morgan Roderick http://roderick.dk
License: MIT - http://mrgnrdrck.mit-license.org

https://github.com/mroderick/PubSubJS
*/
(function (root, factory){
	'use strict';

	var PubSub = {};
	root.PubSub = PubSub;
	factory(PubSub);

	// AMD support
	if (typeof define === 'function' && define.amd){
		define(function() { return PubSub; });

	// CommonJS and Node.js module support
	} else if (typeof exports === 'object'){
		if (module !== undefined && module.exports) {
			exports = module.exports = PubSub; // Node.js specific `module.exports`
		}
		exports.PubSub = PubSub; // CommonJS module 1.1.1 spec
		module.exports = exports = PubSub; // CommonJS
	}

}(( typeof window === 'object' && window ) || this, function (PubSub){
	'use strict';

	var messages = {},
		lastUid = -1;

	function hasKeys(obj){
		var key;

		for (key in obj){
			if ( obj.hasOwnProperty(key) ){
				return true;
			}
		}
		return false;
	}

	/**
	 *	Returns a function that throws the passed exception, for use as argument for setTimeout
	 *	@param { Object } ex An Error object
	 */
	function throwException( ex ){
		return function reThrowException(){
			throw ex;
		};
	}

	function callSubscriberWithDelayedExceptions( subscriber, message, data ){
		try {
			subscriber( message, data );
		} catch( ex ){
			setTimeout( throwException( ex ), 0);
		}
	}

	function callSubscriberWithImmediateExceptions( subscriber, message, data ){
		subscriber( message, data );
	}

	function deliverMessage( originalMessage, matchedMessage, data, immediateExceptions ){
		var subscribers = messages[matchedMessage],
			callSubscriber = immediateExceptions ? callSubscriberWithImmediateExceptions : callSubscriberWithDelayedExceptions,
			s;

		if ( !messages.hasOwnProperty( matchedMessage ) ) {
			return;
		}

		for (s in subscribers){
			if ( subscribers.hasOwnProperty(s)){
				callSubscriber( subscribers[s], originalMessage, data );
			}
		}
	}

	function createDeliveryFunction( message, data, immediateExceptions ){
		return function deliverNamespaced(){
			var topic = String( message ),
				position = topic.lastIndexOf( '.' );

			// deliver the message as it is now
			deliverMessage(message, message, data, immediateExceptions);

			// trim the hierarchy and deliver message to each level
			while( position !== -1 ){
				topic = topic.substr( 0, position );
				position = topic.lastIndexOf('.');
				deliverMessage( message, topic, data, immediateExceptions );
			}
		};
	}

	function messageHasSubscribers( message ){
		var topic = String( message ),
			found = Boolean(messages.hasOwnProperty( topic ) && hasKeys(messages[topic])),
			position = topic.lastIndexOf( '.' );

		while ( !found && position !== -1 ){
			topic = topic.substr( 0, position );
			position = topic.lastIndexOf( '.' );
			found = Boolean(messages.hasOwnProperty( topic ) && hasKeys(messages[topic]));
		}

		return found;
	}

	function publish( message, data, sync, immediateExceptions ){
		var deliver = createDeliveryFunction( message, data, immediateExceptions ),
			hasSubscribers = messageHasSubscribers( message );

		if ( !hasSubscribers ){
			return false;
		}

		if ( sync === true ){
			deliver();
		} else {
			setTimeout( deliver, 0 );
		}
		return true;
	}

	/**
	 *	PubSub.publish( message[, data] ) -> Boolean
	 *	- message (String): The message to publish
	 *	- data: The data to pass to subscribers
	 *	Publishes the the message, passing the data to it's subscribers
	**/
	PubSub.publish = function( message, data ){
		return publish( message, data, false, PubSub.immediateExceptions );
	};

	/**
	 *	PubSub.publishSync( message[, data] ) -> Boolean
	 *	- message (String): The message to publish
	 *	- data: The data to pass to subscribers
	 *	Publishes the the message synchronously, passing the data to it's subscribers
	**/
	PubSub.publishSync = function( message, data ){
		return publish( message, data, true, PubSub.immediateExceptions );
	};

	/**
	 *	PubSub.subscribe( message, func ) -> String
	 *	- message (String): The message to subscribe to
	 *	- func (Function): The function to call when a new message is published
	 *	Subscribes the passed function to the passed message. Every returned token is unique and should be stored if
	 *	you need to unsubscribe
	**/
	PubSub.subscribe = function( message, func ){
		if ( typeof func !== 'function'){
			return false;
		}

		// message is not registered yet
		if ( !messages.hasOwnProperty( message ) ){
			messages[message] = {};
		}

		// forcing token as String, to allow for future expansions without breaking usage
		// and allow for easy use as key names for the 'messages' object
		var token = 'uid_' + String(++lastUid);
		messages[message][token] = func;

		// return token for unsubscribing
		return token;
	};

	/* Public: Clears all subscriptions
	 */
	PubSub.clearAllSubscriptions = function clearAllSubscriptions(){
		messages = {};
	};

	/*Public: Clear subscriptions by the topic
	*/
	PubSub.clearSubscriptions = function clearSubscriptions(topic){
		var m;
		for (m in messages){
			if (messages.hasOwnProperty(m) && m.indexOf(topic) === 0){
				delete messages[m];
			}
		}
	};

	/* Public: removes subscriptions.
	 * When passed a token, removes a specific subscription.
	 * When passed a function, removes all subscriptions for that function
	 * When passed a topic, removes all subscriptions for that topic (hierarchy)
	 *
	 * value - A token, function or topic to unsubscribe.
	 *
	 * Examples
	 *
	 *		// Example 1 - unsubscribing with a token
	 *		var token = PubSub.subscribe('mytopic', myFunc);
	 *		PubSub.unsubscribe(token);
	 *
	 *		// Example 2 - unsubscribing with a function
	 *		PubSub.unsubscribe(myFunc);
	 *
	 *		// Example 3 - unsubscribing a topic
	 *		PubSub.unsubscribe('mytopic');
	 */
	PubSub.unsubscribe = function(value){
		var descendantTopicExists = function(topic) {
				var m;
				for ( m in messages ){
					if ( messages.hasOwnProperty(m) && m.indexOf(topic) === 0 ){
						// a descendant of the topic exists:
						return true;
					}
				}

				return false;
			},
			isTopic    = typeof value === 'string' && ( messages.hasOwnProperty(value) || descendantTopicExists(value) ),
			isToken    = !isTopic && typeof value === 'string',
			isFunction = typeof value === 'function',
			result = false,
			m, message, t;

		if (isTopic){
			PubSub.clearSubscriptions(value);
			return;
		}

		for ( m in messages ){
			if ( messages.hasOwnProperty( m ) ){
				message = messages[m];

				if ( isToken && message[value] ){
					delete message[value];
					result = value;
					// tokens are unique, so we can just stop here
					break;
				}

				if (isFunction) {
					for ( t in message ){
						if (message.hasOwnProperty(t) && message[t] === value){
							delete message[t];
							result = true;
						}
					}
				}
			}
		}

		return result;
	};
}));

var $qs = function(selector, scope) {
	return (scope || document).querySelector(selector);
};

var $qsa = function(selector, scope) {
	return (scope || document).querySelectorAll(selector);
};

function $ajax(url, callback) {
	var request = new XMLHttpRequest();
	request.open('GET', url, true);

	request.onload = function() {
    request.status == 200 ? callback(request.responseText) : callback({ message: 'Error calling ajax' });
	};

	request.onerror = function() {
		console.log('Error calling get-product-vo');
	};

	request.send();
};

var Cookie = (function() {
  var
    get = function(name) {
      var value = "; " + document.cookie;
      var parts = value.split("; " + name + "=");
      if (parts.length == 2) return parts.pop().split(";").shift();
    },

    create = function(name, value) {
      days = 30;
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      var expires = "; expires=" + date.toGMTString();
      document.cookie = name + "=" + value + expires + "; path=/;";
    },

    destroy = function(name) {
      document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;";
    }

    return {
      get: get,
      create: create,
      update: create,
      destroy: destroy,
    }
})();
var autocomplete;

function initAutocomplete() {
	autocomplete = new google.maps.places.Autocomplete(
		(document.getElementById('autocomplete')),
		{types: ['geocode']}
	);

	autocomplete.addListener('place_changed', function() {
		PubSub.publish('address_selected');
	});
}
var Storage = (function() {
  var
    set = function(data) {
      var getCart = JSON.parse(Cookie.get('cart'));

      if(!!getCart.length) {
        var updateCart = getCart.filter(function(item) {
          if(data.id === item.id) return item.qtty = data.qtty;
        });

        if(!!updateCart.length) {
          getCart.concat(updateCart[0]);
        } else {
          getCart.push(data)
        }

        createCookieCart(getCart);

      } else {
        getCart.push(data); 
        createCookieCart(getCart);
      }
    },

    remove = function(data) {
      var getCart = JSON.parse(Cookie.get('cart'));

      var updateCart = getCart.filter(function(item) {
        if(data.id === item.id) {
          if(data.qtty !== 0) {
            return item.qtty = data.qtty;
          } else {
            return false;
          }
        } else {
          return item;
        }
      });

      if(!!updateCart.length) {
        getCart = updateCart;
      } else {
        getCart = [];
      }

      createCookieCart(getCart);

    },

    clear = function() {
      Cookie.create('cart', '[]')
    },

    notify = function() {
      PubSub.publish('cart_udpated');
    },

    createCookieCart = function(data) {
      Cookie.create('cart', JSON.stringify(data));
      notify();
    };
    
    if(!Cookie.get('cart')) Cookie.create('cart', '[]');

    return {
      set: set,
      remove: remove,
      clear: clear
    };
})();

var AgeGate = (function() {
  var hasCookie = Cookie.get('age_gate');

  if(!hasCookie && !!$qs('.modal-age-gate')) {
    var 
      ageModal = $qs('.modal-age-gate'),
      ageButtonYes = $qs('.age-gate-yes'),
      ageButtonNo = $qs('.age-gate-no');

      ageModal.style.display = 'block';
      ageModal.classList.add('open');

      ageButtonYes.addEventListener('click', function() {
        Cookie.create('age_gate', true);
        Modal.close();
      });

      ageButtonNo.addEventListener('click', function() {

        ageButtonYes.style.display = 'none';
        ageButtonNo.style.display = 'none';

        ageModal.querySelector('.modal-subtitle').innerText = 'Você precisa ter mais de 18 anos! :(';

        setTimeout(function() {
          window.location.href = 'https://www.ambev.com.br/consumo-responsavel/';
        }, 1500);
      });
  }
})();

var Card = (function() {
  var initializeCards = function() {
    var
      qttyMore = $qsa('.qtty-more'),
      qttyLess = $qsa('.qtty-less'),
      i = qttyMore.length,
      j = qttyLess.length;
  
    while(i--) {
      qttyMore[i].addEventListener('click', function() {
        var
          parent = this.parentNode.parentNode;
          inputValue = parent.querySelector('input'),
          id = parent.querySelector('.card-qtty').getAttribute('product-id'),
          price = parent.querySelector('.card-price .price').innerText,
          newValue = count('plus', inputValue.value);
        
        inputValue.value = newValue;
        
        Storage.set({id: id, qtty: newValue, price: price});
        
      });
    }
  
    while(j--) {
      qttyLess[j].addEventListener('click', function() {
      var
        parent = this.parentNode,
        inputValue = parent.querySelector('input'),
        id = parent.getAttribute('product-id');
        newValue = count('less', inputValue.value);
      
        inputValue.value = newValue;
  
        Storage.remove({id: id, qtty: newValue});
      });
    }
  
    function count(type, value) {
      var number;
      if(type === 'plus') {
        number = parseFloat(value) + 1;
      } else {
        number =  value > 0 ? number = parseFloat(value) - 1 : number = 0;
      }
  
      return number;
    }
  }

  PubSub.subscribe('list_rendered', initializeCards);

})();


var CustomerAddress = (function() {
	var getAddress = Cookie.get('customer_address');
	
	if(!!getAddress && getAddress !== 'undefined') {
		var
			parse =  JSON.parse(getAddress),
			boxAddress = $qs('.box-address');

		if(!!boxAddress && !!parse) {
			boxAddress.style.display = 'inline-block';
			boxAddress.querySelector('.user-address').innerText = parse.formatedAddress;
		}

		$qs('.change-address').addEventListener('click', function() {
			Cookie.destroy('customer_address');
			Cookie.destroy('sl_id');
			Storage.clear();
		});
	}	
})();


var Header = (function() {
  var burgers = $qs('.burgers');
  if(!!burgers) {
    burgers.addEventListener('click', function(e) {
      var _this = e.target;
  
      _this.classList.toggle('active');
      $qs('.header').classList.toggle('menu-open');
      $qs('.user-actions').classList.toggle('active');
    })
  }  
})();
var FindAddress = (function() {
	var 
		formActions = function() {
			var
				autocomplete = $qs('#autocomplete'),
				pacContainer = $qs('.pac-container');

			autocomplete.addEventListener('focusin', function() {
				autocomplete.classList.add('active');
			});

			autocomplete.addEventListener('focusout', function() {
				autocomplete.classList.remove('active');
			});

			$qs('#find-products').addEventListener('click', function(e) {
				var _this = e.target;

				autocomplete.classList.remove('active');

				if(!!$qs('#number').value) {
					_this.setAttribute('disabled', 'disabled');
          $qs('.find-address').classList.add('loading');
					
          getSellerts(_this);
          
          setTimeout(function() {
            $qs('.find-address').classList.remove('loading');
            resetForm();
          }, 12000);
					
				} else {
          var errorMessage = $qs('.error-message');
          errorMessage.style.display = 'block';
          errorMessage.innerText = 'Preencha o número';

          $qs('#number').addEventListener('focusout', function(e) {
            if(!!e.target.value) {
              errorMessage.style.display = 'none';
            }
          });

          setTimeout(function() {
            errorMessage.style.display = 'none';
          }, 8000);
				}
			});
		}

		getSellerts = function(target) {
      var urlAjax = '/getSellers?lat='+window.data.lat+'&lng='+window.data.lat;
      
      if(!!window.location.href.match("force-match")) {
        urlAjax = '/getSellers?lat=-23.632919&lng=-46.699453';
      }

      $ajax(urlAjax, function(result) {
        var result = JSON.parse(result);
        
        $qs('.find-address').classList.remove('loading');
        target.removeAttribute('disabled');

        if(!!result.pocSearch && !!result.pocSearch.length) {
          Cookie.create('customer_address', JSON.stringify(window.data));
          Cookie.create('sl_id', JSON.stringify(result.pocSearch[0].id));
          window.location.href = '/products';
        } else {
          resetForm();
          Modal.init('modal-seller-not-found');
        }
			});
		}

		completeAddress = function() {
			var place = autocomplete.getPlace();

			$qs('.more-details').style.display = 'block';
			var inputNumber = $qs('#number')

			window.data = {
				formatedAddress: place.formatted_address,
				address: place.name.split(',')[0],
				street_number: place.name.split(',')[1] || null,
				lat: place.geometry ? place.geometry.location.lat() : '',
				lng: place.geometry ? place.geometry.location.lng() : ''
			}

			if(!!data.street_number) {
				inputNumber.value = data.street_number;
			} else {
				inputNumber.value = '';
			}
		}

		resetForm = function() {
			$qs('.more-details').style.display = 'none';
			$qs('#number').value = '';
			$qs('#autocomplete').value = '';
		}

	if(!!$qs('.find-address')) formActions();

	PubSub.subscribe('address_selected', completeAddress );

})();
var MiniCart = (function() {
	var updateCart = function() {
		var getCart = JSON.parse(Cookie.get('cart'));

		var totalOrder = getCart.reduce(function(accumulator, currentValue, currentIndex, array) {
				return accumulator + (currentValue.price * currentValue.qtty );
		}, 0);

		var totalItems = getCart.reduce(function(accumulator, currentValue, currentIndex, array) {
			return accumulator + (currentValue.qtty);
    }, 0);
    
		if(!!totalOrder) {
			$qs('.mini-cart .box-value').style.display = 'inline-block';
			$qs('.mini-cart .value').innerText = parseFloat(totalOrder).toFixed(2);
		} else {
      $qs('.mini-cart .box-value').style.display = 'none';
    }

		if(!!totalItems) {
			$qs('.mini-cart .count').style.display = 'inline-block';
			$qs('.mini-cart .count').innerText = totalItems;
		} else {
      $qs('.mini-cart .count').innerText = '0';
    }
	}

	var miniCart = $qs('.mini-cart');
	if(!!miniCart) {
		updateCart();

		PubSub.subscribe('cart_udpated', function(){
			updateCart();
		});
  }
})();


var Modal = (function() {
  var
    init = function(target) {
      var
        modal = $qs('.'+target);

      modal.classList.add('open');
      modal.style.display = 'block';

      closeButton(modal);
    }

    closeButton = function(element) {
      var 
        modal = element,
        closeAction = modal.querySelectorAll('.close'),
        i = closeAction.length;

      while(i--) {
        closeAction[i].addEventListener('click', function() {
          modal.classList.remove('open');
          modal.style.display = 'none';
        });
      }
    },

    close = function() {
      var 
        modal = $qsa('.modal'),
        i = modal.length;

      while(i--) {
        if(modal[i].classList.contains('open')) {
          modal[i].classList.remove('open');
          modal[i].style.display = 'none';
        }
      }
    }

    return {
      init: init,
      close: close
    }
})();



var List = (function() {
  PubSub.subscribe('list_rendered', function(){
    if(!!$qs('.box-error')) {
      $qs('.btn-back-empty-search').addEventListener('click', function(e) {
        e.preventDefault();
        Cookie.destroy('customer_address');
        window.location.href = '/';
      });
    }
  });
})();
var Products = (function() {
  
	var getProducts = function() {
    var sellerId = Cookie.get('sl_id') || '';
		$ajax('/getProducts?sl_id='+sellerId, function(result) {
      $qs('.card-list').innerHTML = result;

      PubSub.publish('list_rendered');
		});
  }

	if(!!$qs('.search')) getProducts();
})();