# Code challange ZX Ventures

## Executando o projeto

Para executar o projeto é necessário ter:

```
- Gulpjs
- Nodejs >= 7.0.0
- NPM
```

Caso necessário instale as dependências para rodar o projeto.

## Subindo o projeto

Para subir o projeto é necessario executar o seguinte comando na raiz do projeto: `npm start`

## Rodando versão de desenvolvimento
Para rodar a versao de desenvolvimento executar:

### Nodejs
Na raiz do projeto executar:
```node app.js```

caso você possua o `nodemon` você pode executar:
```nodemon app.js```

### Assets
Na raiz do projeto executar:
```gulp watch```

## Gerando assets para deploy
Para gerar os arquivos de produção executar:
```gulp build```

## Testes
Para visualizar os testes no terminal executar:
```gulp test```

Será aberto um link com todos os códigos e coberturas.