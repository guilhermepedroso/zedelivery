var List = (function() {
  PubSub.subscribe('list_rendered', function(){
    if(!!$qs('.box-error')) {
      $qs('.btn-back-empty-search').addEventListener('click', function(e) {
        e.preventDefault();
        Cookie.destroy('customer_address');
        window.location.href = '/';
      });
    }
  });
})();