var Products = (function() {
  
	var getProducts = function() {
    var sellerId = Cookie.get('sl_id') || '';
		$ajax('/getProducts?sl_id='+sellerId, function(result) {
      $qs('.card-list').innerHTML = result;

      PubSub.publish('list_rendered');
		});
  }

	if(!!$qs('.search')) getProducts();
})();