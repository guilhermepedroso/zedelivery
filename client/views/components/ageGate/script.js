var AgeGate = (function() {
  var hasCookie = Cookie.get('age_gate');

  if(!hasCookie && !!$qs('.modal-age-gate')) {
    var 
      ageModal = $qs('.modal-age-gate'),
      ageButtonYes = $qs('.age-gate-yes'),
      ageButtonNo = $qs('.age-gate-no');

      ageModal.style.display = 'block';
      ageModal.classList.add('open');

      ageButtonYes.addEventListener('click', function() {
        Cookie.create('age_gate', true);
        Modal.close();
      });

      ageButtonNo.addEventListener('click', function() {

        ageButtonYes.style.display = 'none';
        ageButtonNo.style.display = 'none';

        ageModal.querySelector('.modal-subtitle').innerText = 'Você precisa ter mais de 18 anos! :(';

        setTimeout(function() {
          window.location.href = 'https://www.ambev.com.br/consumo-responsavel/';
        }, 1500);
      });
  }
})();
