var Header = (function() {
  var burgers = $qs('.burgers');
  if(!!burgers) {
    burgers.addEventListener('click', function(e) {
      var _this = e.target;
  
      _this.classList.toggle('active');
      $qs('.header').classList.toggle('menu-open');
      $qs('.user-actions').classList.toggle('active');
    })
  }  
})();