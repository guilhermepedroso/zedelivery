var FindAddress = (function() {
	var 
		formActions = function() {
			var
				autocomplete = $qs('#autocomplete'),
				pacContainer = $qs('.pac-container');

			autocomplete.addEventListener('focusin', function() {
				autocomplete.classList.add('active');
			});

			autocomplete.addEventListener('focusout', function() {
				autocomplete.classList.remove('active');
			});

			$qs('#find-products').addEventListener('click', function(e) {
				var _this = e.target;

				autocomplete.classList.remove('active');

				if(!!$qs('#number').value) {
					_this.setAttribute('disabled', 'disabled');
          $qs('.find-address').classList.add('loading');
					
          getSellerts(_this);
          
          setTimeout(function() {
            $qs('.find-address').classList.remove('loading');
            resetForm();
          }, 12000);
					
				} else {
          var errorMessage = $qs('.error-message');
          errorMessage.style.display = 'block';
          errorMessage.innerText = 'Preencha o número';

          $qs('#number').addEventListener('focusout', function(e) {
            if(!!e.target.value) {
              errorMessage.style.display = 'none';
            }
          });

          setTimeout(function() {
            errorMessage.style.display = 'none';
          }, 8000);
				}
			});
		}

		getSellerts = function(target) {
      var urlAjax = '/getSellers?lat='+window.data.lat+'&lng='+window.data.lat;
      
      if(!!window.location.href.match("force-match")) {
        urlAjax = '/getSellers?lat=-23.632919&lng=-46.699453';
      }

      $ajax(urlAjax, function(result) {
        var result = JSON.parse(result);
        
        $qs('.find-address').classList.remove('loading');
        target.removeAttribute('disabled');

        if(!!result.pocSearch && !!result.pocSearch.length) {
          Cookie.create('customer_address', JSON.stringify(window.data));
          Cookie.create('sl_id', JSON.stringify(result.pocSearch[0].id));
          window.location.href = '/products';
        } else {
          resetForm();
          Modal.init('modal-seller-not-found');
        }
			});
		}

		completeAddress = function() {
			var place = autocomplete.getPlace();

			$qs('.more-details').style.display = 'block';
			var inputNumber = $qs('#number')

			window.data = {
				formatedAddress: place.formatted_address,
				address: place.name.split(',')[0],
				street_number: place.name.split(',')[1] || null,
				lat: place.geometry ? place.geometry.location.lat() : '',
				lng: place.geometry ? place.geometry.location.lng() : ''
			}

			if(!!data.street_number) {
				inputNumber.value = data.street_number;
			} else {
				inputNumber.value = '';
			}
		}

		resetForm = function() {
			$qs('.more-details').style.display = 'none';
			$qs('#number').value = '';
			$qs('#autocomplete').value = '';
		}

	if(!!$qs('.find-address')) formActions();

	PubSub.subscribe('address_selected', completeAddress );

})();