var MiniCart = (function() {
	var updateCart = function() {
		var getCart = JSON.parse(Cookie.get('cart'));

		var totalOrder = getCart.reduce(function(accumulator, currentValue, currentIndex, array) {
				return accumulator + (currentValue.price * currentValue.qtty );
		}, 0);

		var totalItems = getCart.reduce(function(accumulator, currentValue, currentIndex, array) {
			return accumulator + (currentValue.qtty);
    }, 0);
    
		if(!!totalOrder) {
			$qs('.mini-cart .box-value').style.display = 'inline-block';
			$qs('.mini-cart .value').innerText = parseFloat(totalOrder).toFixed(2);
		} else {
      $qs('.mini-cart .box-value').style.display = 'none';
    }

		if(!!totalItems) {
			$qs('.mini-cart .count').style.display = 'inline-block';
			$qs('.mini-cart .count').innerText = totalItems;
		} else {
      $qs('.mini-cart .count').innerText = '0';
    }
	}

	var miniCart = $qs('.mini-cart');
	if(!!miniCart) {
		updateCart();

		PubSub.subscribe('cart_udpated', function(){
			updateCart();
		});
  }
})();

