var CustomerAddress = (function() {
	var getAddress = Cookie.get('customer_address');
	
	if(!!getAddress && getAddress !== 'undefined') {
		var
			parse =  JSON.parse(getAddress),
			boxAddress = $qs('.box-address');

		if(!!boxAddress && !!parse) {
			boxAddress.style.display = 'inline-block';
			boxAddress.querySelector('.user-address').innerText = parse.formatedAddress;
		}

		$qs('.change-address').addEventListener('click', function() {
			Cookie.destroy('customer_address');
			Cookie.destroy('sl_id');
			Storage.clear();
		});
	}	
})();

