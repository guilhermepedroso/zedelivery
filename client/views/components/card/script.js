var Card = (function() {
  var initializeCards = function() {
    var
      qttyMore = $qsa('.qtty-more'),
      qttyLess = $qsa('.qtty-less'),
      i = qttyMore.length,
      j = qttyLess.length;
  
    while(i--) {
      qttyMore[i].addEventListener('click', function() {
        var
          parent = this.parentNode.parentNode;
          inputValue = parent.querySelector('input'),
          id = parent.querySelector('.card-qtty').getAttribute('product-id'),
          price = parent.querySelector('.card-price .price').innerText,
          newValue = count('plus', inputValue.value);
        
        inputValue.value = newValue;
        
        Storage.set({id: id, qtty: newValue, price: price});
        
      });
    }
  
    while(j--) {
      qttyLess[j].addEventListener('click', function() {
      var
        parent = this.parentNode,
        inputValue = parent.querySelector('input'),
        id = parent.getAttribute('product-id');
        newValue = count('less', inputValue.value);
      
        inputValue.value = newValue;
  
        Storage.remove({id: id, qtty: newValue});
      });
    }
  
    function count(type, value) {
      var number;
      if(type === 'plus') {
        number = parseFloat(value) + 1;
      } else {
        number =  value > 0 ? number = parseFloat(value) - 1 : number = 0;
      }
  
      return number;
    }
  }

  PubSub.subscribe('list_rendered', initializeCards);

})();

