var Modal = (function() {
  var
    init = function(target) {
      var
        modal = $qs('.'+target);

      modal.classList.add('open');
      modal.style.display = 'block';

      closeButton(modal);
    }

    closeButton = function(element) {
      var 
        modal = element,
        closeAction = modal.querySelectorAll('.close'),
        i = closeAction.length;

      while(i--) {
        closeAction[i].addEventListener('click', function() {
          modal.classList.remove('open');
          modal.style.display = 'none';
        });
      }
    },

    close = function() {
      var 
        modal = $qsa('.modal'),
        i = modal.length;

      while(i--) {
        if(modal[i].classList.contains('open')) {
          modal[i].classList.remove('open');
          modal[i].style.display = 'none';
        }
      }
    }

    return {
      init: init,
      close: close
    }
})();
