var $qs = function(selector, scope) {
	return (scope || document).querySelector(selector);
};

var $qsa = function(selector, scope) {
	return (scope || document).querySelectorAll(selector);
};

function $ajax(url, callback) {
	var request = new XMLHttpRequest();
	request.open('GET', url, true);

	request.onload = function() {
    request.status == 200 ? callback(request.responseText) : callback({ message: 'Error calling ajax' });
	};

	request.onerror = function() {
		console.log('Error calling get-product-vo');
	};

	request.send();
};
