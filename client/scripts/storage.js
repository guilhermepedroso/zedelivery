var Storage = (function() {
  var
    set = function(data) {
      var getCart = JSON.parse(Cookie.get('cart'));

      if(!!getCart.length) {
        var updateCart = getCart.filter(function(item) {
          if(data.id === item.id) return item.qtty = data.qtty;
        });

        if(!!updateCart.length) {
          getCart.concat(updateCart[0]);
        } else {
          getCart.push(data)
        }

        createCookieCart(getCart);

      } else {
        getCart.push(data); 
        createCookieCart(getCart);
      }
    },

    remove = function(data) {
      var getCart = JSON.parse(Cookie.get('cart'));

      var updateCart = getCart.filter(function(item) {
        if(data.id === item.id) {
          if(data.qtty !== 0) {
            return item.qtty = data.qtty;
          } else {
            return false;
          }
        } else {
          return item;
        }
      });

      if(!!updateCart.length) {
        getCart = updateCart;
      } else {
        getCart = [];
      }

      createCookieCart(getCart);

    },

    clear = function() {
      Cookie.create('cart', '[]')
    },

    notify = function() {
      PubSub.publish('cart_udpated');
    },

    createCookieCart = function(data) {
      Cookie.create('cart', JSON.stringify(data));
      notify();
    };
    
    if(!Cookie.get('cart')) Cookie.create('cart', '[]');

    return {
      set: set,
      remove: remove,
      clear: clear
    };
})();
