var Cookie = (function() {
  var
    get = function(name) {
      var value = "; " + document.cookie;
      var parts = value.split("; " + name + "=");
      if (parts.length == 2) return parts.pop().split(";").shift();
    },

    create = function(name, value) {
      days = 30;
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      var expires = "; expires=" + date.toGMTString();
      document.cookie = name + "=" + value + expires + "; path=/;";
    },

    destroy = function(name) {
      document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;";
    }

    return {
      get: get,
      create: create,
      update: create,
      destroy: destroy,
    }
})();