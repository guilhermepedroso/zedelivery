var autocomplete;

function initAutocomplete() {
	autocomplete = new google.maps.places.Autocomplete(
		(document.getElementById('autocomplete')),
		{types: ['geocode']}
	);

	autocomplete.addListener('place_changed', function() {
		PubSub.publish('address_selected');
	});
}