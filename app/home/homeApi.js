const hbs = require('express-hbs');
const graphql = require('graphql');
const graphqlClient = require('graphql-client');
const cookie = require('cookie');

const config = require('../../config');
const pocSchema = require('./pocSearchSchema');

function getSeller(req, res) {
	const variables = {
    algorithm: 'NEAREST',
    lat: req.query.lat,
    long: req.query.lng,
    now: new Date().toISOString()
  }

  config.client.query(pocSchema, variables, function(req, res) {
    if(res.status === 401) { throw new Error('Not authorized') }
  })
  .then(function(body) {
    return res.status(200).send(body.data);
  })
  .catch(function(err) {
    return res.status(400).send(err.message);
  })
}

function home(req, res) {
  if(!!req.headers.cookie) {
    const getCookie = cookie.parse(req.headers.cookie);
    if(!!getCookie.customer_address) return res.redirect('/products');
  }

  return res.render('home/index');
}

module.exports = {
	home,
	getSeller
};
