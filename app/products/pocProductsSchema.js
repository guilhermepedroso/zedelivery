module.exports = `
query pocCategorySearch($id: ID!, $search: String!, $categoryId: Int!) {
  poc(id: $id) {
    products(categoryId: $categoryId, search: $search) {
      productVariants{
        productVariantId
        title
        description
        imageUrl
        price
      }
    }
  }
}
`;