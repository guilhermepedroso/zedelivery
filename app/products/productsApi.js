const hbs = require('express-hbs');
const graphql = require('graphql');
const graphqlClient = require('graphql-client');
const cookie = require('cookie');

const config = require('../../config');
const pocSchema = require('./pocProductsSchema');

function getProducts(req, res) {
	const getCookies = cookie.parse(req.headers.cookie);
	const getCartCookie = getCookies.cart;
	const getSellerId = getCookies.sl_id;

	var variables = {
		id: getSellerId,
		search: '',
		categoryId: 0
	};

	config.client
		.query(pocSchema, variables, function(req, res) {
			if (res.status === 401) {
				throw new Error('Not authorized');
			}
		})
		.then(function(result) {
      if(!result.data.poc) return res.render('list/index', { error: true });

      const cookieData = cookie.parse(req.headers.cookie);
			const cartData = JSON.parse(cookieData.cart);
      const data = result.data.poc.products;
		
			data.map((item) => {
				const { productVariants } = item || [];
				productVariants.map((productVariant) => {
					const filtered = cartData.filter((product) => product.id === productVariant.productVariantId)[0];
					if (filtered) productVariant['qtty'] = filtered.qtty;
				});
      });
			return res.render('list/index', { products: data });
		})
		.catch(function(err) {
			return res.status(400).send(err.message);
		});
}


function product(req, res) {
  if(!!req.headers.cookie) {
    const getCookie = cookie.parse(req.headers.cookie);
    if(!getCookie.customer_address) return res.redirect('/');
  }

	return res.render('products/index');
}

module.exports = {
	product,
	getProducts
};
