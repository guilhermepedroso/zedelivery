const path = require('path');
const express = require('express');
const hbs = require('express-hbs');
const bodyParser = require('body-parser');

const config = require('./config');
const home = require ('./app/home/homeApi');
const product = require('./app/products/productsApi')

const app = express();

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'hbs');
app.set('views', __dirname + '/client/views/pages');

app.engine('hbs', hbs.express4({
  partialsDir: path.join(__dirname + '/client/views/components')
}));

// Home
app.get('/', home.home);
app.get('/getSellers', home.getSeller);

// Products
app.get('/products', product.product);
app.get('/getProducts', product.getProducts);


// 404
app.get('*', function(req, res){
  res.render('404/index');
});

app.listen(3009);


console.log(`
+-----------------------------------------+
| Express LISTEN: [http://localhost:3009] |
+-----------------------------------------+
`);

module.exports = {
  app,
};
