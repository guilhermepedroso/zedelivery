describe('PubSub', function() {
  it('must have pubSub avaiable', function() {
    expect(!!typeof PubSub).to.be(true);
  });

  it('should publis an event', function() {
    PubSub.publish('test_pub');
    PubSub.subscribe('test_pub', function(){
			expect(true).to.be.ok();
		});
  });
});