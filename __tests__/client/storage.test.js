describe('Storage', function() {
  it('must have cart cookie avaiable', function() {
    var cookie = !!Cookie.get('cart');
    expect(cookie).to.eql(true);
  });
  it('should add a new item in cookie', function() {
    Storage.set({"id":"1341","qtty":3,"price":"20.28"});
    expect(Cookie.get('cart')).to.eql('[{"id":"1341","qtty":3,"price":"20.28"}]');
  });
  it('should update a item in cookie', function() {
    Storage.set({"id":"1341","qtty":1,"price":"20.28"});
    expect(Cookie.get('cart')).to.eql('[{"id":"1341","qtty":1,"price":"20.28"}]');
  });
  it('should remove a new item in cookie', function() {
    Storage.remove({"id":"1341","qtty":0,"price":"20.28"});
    expect(Cookie.get('cart')).to.eql('[]');
  });
});