describe('Cookie', function() {
  it('must have the cookie function available', function() {
    expect(typeof Cookie).to.eql("object")
  });
  it('should create a cookie', function() {
    Cookie.create('teste', 'abc');
    expect(Cookie.get('teste')).to.eql('abc')
  });
  it('should update test cookie', function() {
    Cookie.create('teste', 'trololo');
    expect(Cookie.get('teste')).to.eql('trololo');
  });
  it('should get a cookie', function() {
    Cookie.create('zx', 'venture');
    expect(Cookie.get('zx')).to.eql('venture');
  });
  it('should remove test cookie', function() {
    Cookie.destroy('teste');
    expect(!!Cookie.get('teste')).to.eql(false);
  });
});