describe('Utils', function() {
  it('should get one element', function() {
    var html = '<h1>Ola</h1>';

    $qs('#mock').insertAdjacentHTML('afterbegin', html);
    expect(!!$qs('#mock')).to.eql(true);
    $qs('#mock').innerHTML = '';
  });
  it('should get all elements', function() {
    var html = '<span>ola</span><span>ola</span><span>ola</span><span>ola</span>';

    $qs('#mock').insertAdjacentHTML('afterbegin', html);
    expect($qsa('#mock span').length).to.be(4)
    $qs('#mock').innerHTML = '';
  });
});