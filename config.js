const graphql = require('graphql');
const graphqlClient = require('graphql-client');

const client = graphqlClient({
	url: 'https://803votn6w7.execute-api.us-west-2.amazonaws.com/dev/public/graphql',
	headers: {}
});

module.exports = {
	client
};