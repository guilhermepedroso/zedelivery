const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const imagemin = require('gulp-imagemin');
const del = require('del');
const webserver = require('gulp-webserver');
const autoprefixer = require('gulp-autoprefixer');
const runSequence = require('run-sequence');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');

gulp.task('scripts', function() {
  return gulp.src([
    './client/scripts/vendor/**/*.js',
    './client/scripts/utils.js',
    './client/scripts/cookie.js',
    './client/scripts/gmaps.js',
    './client/scripts/storage.js',
    './client/scripts/app.js',
    './client/views/**/*.js',
    ])
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('./public/js'))
});

gulp.task('minify-js', function() {
  return gulp.src([
    './client/scripts/vendor/**/*.js',
    './client/scripts/utils.js',
    './client/scripts/cookie.js',
    './client/scripts/gmaps.js',
    './client/scripts/storage.js',
    './client/scripts/app.js',
    './client/views/**/*.js',
    ])
    .pipe(concat('bundle.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'))
 });

gulp.task('sass', function () {
  return gulp.src([
      './client/styles/**/*.scss',
      './client/views/**/*.scss',
    ])
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('styles.css'))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./public/css'))
});

gulp.task('minify-css', () => {
  return gulp.src('./public/css/styles.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./public/css/'));
});

gulp.task('images', function(){
  return gulp.src('./client/images/**/*.+(png|jpg|gif|svg)')
  .pipe(imagemin())
  .pipe(gulp.dest('public/images'))
});

gulp.task('clean', function() {
  return del.sync('public');
});

gulp.task('watch', ['images', 'sass', 'scripts'], function (){
  gulp.watch(['./client/views/**/*.scss', './client/styles/**/*.scss'], ['sass']); 
	gulp.watch(['./client/views/**/*.js', './client/scripts/**/*.js'], ['scripts']);
	gulp.watch('./client/images/**/*{png|jpg|gif|svg}', ['images']);
});

gulp.task('build', function(cb) {
  runSequence('clean', 'images', 'sass', 'scripts', 'minify-css', 'minify-js', cb);
});

gulp.task('test', function() {
  gulp.src('./public/js/bundle.js')
  .pipe(gulp.dest('__tests__/'));

  gulp.src('__tests__')
    .pipe(webserver({
      livereload: false,
      directoryListing: false,
      open: true
    }));
});